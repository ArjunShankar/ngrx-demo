import {Injectable} from '@angular/core';
import {UserService} from '../services/user.service';
import {Effect, Actions} from '@ngrx/effects';
import * as userActions from '../actions/user.actions';
import { switchMap, map } from 'rxjs/operators';



@Injectable()
export class UserEffects {
  constructor(private userService: UserService,
              private actions$: Actions) {

  }

  @Effect() loadUsers$ = this.actions$
    .ofType(userActions.LOAD_USERS)
    .pipe(
      switchMap(() => this.userService.getUserData()
                              .pipe(
                                map(response => new userActions.LoadUsersSuccessAction(response))
                              )
      )
    );

  @Effect() deleteUser$ = this.actions$
    .ofType(userActions.DELETE_USER)
    .pipe(
      switchMap(action => this.userService.deleteUser(action['payload'])
        .pipe(
          map(response => new userActions.DeleteUserSuccessAction(response.id))
        )
      )
    );
}
