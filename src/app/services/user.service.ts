import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  getUserData(): Observable<any> {
    const url = 'https://jsonplaceholder.typicode.com/users';
    return this.http.get(url);
      /*
      .pipe(
        map((res: any) => {
          return res;
        })//,
       // catchError(err => {console.log(err)})
      );*/

  }

  deleteUser(input: number): Observable<any> {
    const url = 'https://jsonplaceholder.typicode.com/users' + '/' + input;
    return this.http.delete(url);
    /*
    .pipe(
      map((res: any) => {
        return res;
      })//,
     // catchError(err => {console.log(err)})
    );*/

  }

  errorHandler(err: any) {
    console.log(err);
  }
}
