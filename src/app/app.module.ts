import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { ServiceWorkerModule } from '@angular/service-worker';
import { AppComponent } from './app.component';

import { environment } from '../environments/environment';
import { UsersListComponent } from './components/users-list/users-list.component';

import {HttpClientModule} from '@angular/common/http';
import {MatIconModule, MatTableModule} from '@angular/material';

import {StoreModule} from '@ngrx/store';
import {userReducer} from './reducers/user.reducer';
import {UserService} from './services/user.service';
import {EffectsModule} from '@ngrx/effects';
import {UserEffects} from './effects/user.effects';
import { GridComponent } from './components/grid/grid.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

@NgModule({
  declarations: [
    AppComponent,
    UsersListComponent,
    GridComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatTableModule,
    MatIconModule,
    StoreModule.forRoot({users: userReducer}),
    EffectsModule.forRoot([UserEffects]),
    StoreDevtoolsModule.instrument(),
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
