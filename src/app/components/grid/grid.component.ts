import {Component, OnInit, Input, ChangeDetectionStrategy, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GridComponent implements OnInit {
  public displayedColumns = ['id', 'name', 'email', 'action'];
  @Input() dataProvider: Array<any>;
  @Output() deleteUser: EventEmitter<any>;

  constructor() {
    this.deleteUser = new EventEmitter<any>();
  }

  ngOnInit() {
  }

  delete(input: number) {
    this.deleteUser.emit(input);
  }

}
