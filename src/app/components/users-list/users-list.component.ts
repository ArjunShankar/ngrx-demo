import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../models/app-state';
import {User} from '../../models/user';
import {Observable} from 'rxjs/Observable';

import * as userActions from '../../actions/user.actions';
import { map} from 'rxjs/operators';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  public dP$: Observable<any>


  constructor(
    private store: Store<AppState>) { }

  ngOnInit() {
    this.dP$ = this.store.select( state => state.users);
    this.getData();
  }

  getData() {
    this.store.dispatch(new userActions.LoadUsersAction());
  }

  deleteUser(userID: number) {
    this.store.dispatch(new userActions.DeleteUserAction(userID));
  }



}
