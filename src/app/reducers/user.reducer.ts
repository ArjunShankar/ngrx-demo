import * as userActions from '../actions/user.actions'; // kind of a namespace...

export function userReducer(state = [], action: userActions.Action) { // the state can be any JS object. Using array for simplicity
  switch (action.type) {
    case userActions.LOAD_USERS_SUCCESS: {
      return action.payload;
    }
    case userActions.DELETE_USER: {
      return state.filter(user => user.id !== action.payload);
    }
    default: {
      return state;
    }
  }

}
